import BookList from "./components/bookList";
import SongList from "./components/songList";
import ThemeContextProvider from "./contexts/ThemeContext";
import Counter from "./components/Counter";
import ClassCounter from "./components/ClassCounter";
import Hoc from "./HOC";
import StockList from "./StockList";
import UserList from "./UserList";

const StocksData = [
  {
    id: 1,
    name: "TCS",
  },
  {
    id: 2,
    name: "Infosys",
  },
  {
    id: 3,
    name: "Reliance",
  },
];
const UsersData = [
  {
    id: 1,
    name: "Krunal",
  },
  {
    id: 2,
    name: "Ankit",
  },
  {
    id: 3,
    name: "Rushabh",
  },
];

const Stocks = Hoc(StockList, StocksData);

const Users = Hoc(UserList, UsersData);

function App() {
  const song = [{ title: "Song #1" }, { title: "Song #2" }];
  return (
    <div className="App" style={{ display: "flex", justifyContent: "center" }}>
      {/* <ThemeContextProvider data={song}>
          <SongList />
          <BookList />
        </ThemeContextProvider> */}
      {/* <Counter></Counter> */}
      {/* <ClassCounter></ClassCounter> */}
      <Users></Users>
      <Stocks></Stocks>
    </div>
  );
}

App = Hoc(App);
export default App;
