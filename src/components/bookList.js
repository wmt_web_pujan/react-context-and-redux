import React, { useState } from "react";

export default function BookList() {
  const [book, setBook] = useState([
    { title: "book1" },
    { title: "book2" },
    { title: "book3" },
    { title: "book4" },
  ]);
  return (
    <div>
      {book?.map((e) => (
        <div style={{ padding: "10px" }}>{e.title}</div>
      ))}
    </div>
  );
}
