import React, { useContext } from "react";
import { ThemeContext } from "../contexts/ThemeContext";

export default function SongList2() {
  const song = useContext(ThemeContext);
  console.log(song, "useContext");
  return (
    <div>
      <ThemeContext.Consumer>
        {(context) => {
          console.log(context, "context");
          return song?.map((e) => (
            <div style={{ padding: "10px" }}>{e.title}</div>
          ));
        }}  
      </ThemeContext.Consumer>
    </div>
  );
}
