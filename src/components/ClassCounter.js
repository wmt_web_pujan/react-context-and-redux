import React, { Component } from "react";
import { connect } from "react-redux";
import { decrement, increment } from "../redux/actions/couterAction";
import { storeUserData } from "../redux/actions/authAction";

const user = {
  name: "test er",
  username: "test",
  email: "test@gmail.com",
};

class ClassCounter extends Component {
  render() {
    return (
      <div>
        {/* {console.log(process.env.REACT_APP_API_KEY, "helo")} */}
        <h1>{this.props.count.value}</h1>
        <button onClick={() => this.props.increment()}>+</button>
        <button onClick={() => this.props.decrement()}>-</button>

        <hr />
        <h1>User Dispatch</h1>
        <button onClick={() => this.props.storeUserData(user)}>User</button>

        
      </div>
    );
  }
}

const mapStateToProps = (state) => ({ 
  ...state,
});

// const mapDispatchToProps = (dispatch) => ({
//   increment: () => dispatch(increment()),
//   decrement: () => dispatch(decrement()),
//   storeUserData: (payload) => dispatch(storeUserData(payload)),
// });

// export default connect(mapStateToProps, mapDispatchToProps)(ClassCounter);
export default connect(mapStateToProps, {
  increment,
  decrement,
  storeUserData,
})(ClassCounter);
