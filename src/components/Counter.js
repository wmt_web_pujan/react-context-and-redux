import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { decrement, increment } from "../redux/actions/couterAction";
import { storeUserData } from "../redux/actions/authAction";

export default function Counter() {
  const counter = useSelector((state) => state.count);
  const authUser = useSelector((state) => state.user);
  const dispatch = useDispatch();

  const user = {
    name: "test user",
    username: "test",
    email: "test@gmail.com",
  };

  return (
    <div>
      <h1>{counter.value}</h1>
      <button onClick={() => dispatch(increment())}>+</button>
      <button onClick={() => dispatch(decrement())}>-</button>

      <hr />
      <h1>User Dispatch</h1>
      <button onClick={() => dispatch(storeUserData(user))}>User</button>
      {console.log(authUser, "helo")}
    </div>
  );
}
