import { USER } from "../type";

export const storeUserData = (data) => {
  return {
    type: USER,
    payload: data,
  };
};

export default {
  storeUserData,
};
