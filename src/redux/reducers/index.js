import { combineReducers } from "redux";
import couterReducer from "./counterReducer";
import authReducer from "./authReducer";

const rootReducer = combineReducers({
  user: authReducer,
  count: couterReducer,
});

export default rootReducer;
