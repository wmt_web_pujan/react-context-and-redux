import { USER } from "../type";

const initialState = {
  user: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case USER:
      return action.payload;
    default:
      return state;
  }
};
