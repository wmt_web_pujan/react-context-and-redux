import React from "react";

// export default function Hoc(HocComponent, data) {
//   return class extends Component {
//     constructor(props) {
//       super(props);
//       this.state = {
//         data: data,
//       };
//     }

//     render() {
//       return <HocComponent data={this.state.data} {...this.props} />;
//     }
//   };
// }

const Hoc = (HocComponent, data) => (props) => {
  return (
    <div>
      <HocComponent data={data} {...props} />
    </div>
  );
};

export default Hoc;
