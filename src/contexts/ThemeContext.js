import React, { createContext, useState } from "react";

export const ThemeContext = createContext();

export default function ThemeContextProvider({ children, data }) {
  return (
    <div>
      <ThemeContext.Provider value={data}>{children}</ThemeContext.Provider>
    </div>
  );
}
